<?php

use DeviceDetector\DeviceDetector;

class DetectAgentTest extends \PHPUnit\Framework\TestCase
{
    public function testDetect(): void
    {
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36';

        $dd = new DeviceDetector($userAgent);

        $this->assertFalse($dd->isBot());
        $this->assertFalse($dd->isBrowser());
        $this->assertNull($dd->getOs());
        $this->assertNull($dd->getClient());

        $dd->parse();
        $this->assertFalse($dd->isBot());
        $this->assertTrue($dd->isBrowser());
        $this->assertSame([
            'name' => 'GNU/Linux',
            'short_name' => 'LIN',
            'version' => '',
            'platform' => 'x64',
            'family' => 'GNU/Linux',
        ],$dd->getOs());
        $this->assertSame([
            'type' => 'browser',
            'name' => 'Chrome',
            'short_name' => 'CH',
            'version' => '126.0',
            'engine' => 'Blink',
            'engine_version' => '126.0.0.0',
            'family' => 'Chrome',
        ], $dd->getClient());
    }

    public function testDetectBot(): void
    {
        $userAgent = 'Mozilla/5.0 (compatible; AhrefsBot/7.0; +http://ahrefs.com/robot/)';

        $dd = new DeviceDetector($userAgent);

        $this->assertFalse($dd->isBot());
        $this->assertFalse($dd->isBrowser());
        $this->assertNull($dd->getOs());
        $this->assertNull($dd->getClient());

        $dd->parse();
        $this->assertTrue($dd->isBot());
        $this->assertFalse($dd->isBrowser());
        $this->assertNull($dd->getOs());
        $this->assertNull($dd->getClient());
        $this->assertSame([
            'name' => 'aHrefs Bot',
            'category' => 'Crawler',
            'url' => 'https://ahrefs.com/robot',
            'producer' => [
                'name' => 'Ahrefs Pte Ltd',
                'url' => 'https://ahrefs.com/robot',
            ],
        ], $dd->getBot());
    }
}
